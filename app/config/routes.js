const React = require('react');
const ReactRouter = require('react-router');
const Router = ReactRouter.Router;
const Route = ReactRouter.Route;
const IndexRoute = ReactRouter.IndexRoute;
const hashHistory = ReactRouter.hashHistory;
const Main = require('../components/Main');
const Home = require('../components/Home');
const ImageOfDay = require('../components/ImageOfDay');
const ImageGallery = require('../components/ImageGallery');

const routes = (
    <Router history={hashHistory}>
        <Route path='/' component={Main}>
            <IndexRoute component={ImageOfDay} />
            <Route path='/imageOfDay' component={ImageOfDay} />
            <Route path='/imageGallery' component={ImageGallery} />
        </Route>
    </Router>
);

module.exports = routes;
