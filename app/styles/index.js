const styles = {
    transparentBg: {
        background: 'transparent'
    },
    space: {
        marginTop: "25px"
    },
    appHeader: {
        display: "flex",
        justifyContent: "space-between"
    },
    buttonContainer : {
        display: "flex",
        alignItems: "center"
    },
    image : {
        width: "85%",
        height: "500px"
    }
}

module.exports = styles;
