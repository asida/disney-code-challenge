const React = require('react');
const Header = require('./Header');

const ImageGallery = React.createClass({
    render: function() {
        return (
            <div className="col-sm-12 text-center">
                <Header title="The Red Planet Rovers" btnOneTitle="Image of the Day" btnTwoTitle="Image Gallery"/>
                <h1>Image Gallery!</h1>
            </div>
        )
    }
});

module.exports = ImageGallery;
