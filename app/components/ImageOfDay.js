const React = require('react');
const axios = require('axios');
const styles = require('../styles');
const Header = require('./Header');
const apiHelper = require('../../utils/apiHelper');

const key = "aZlmHCp3jD9sanwE8KvytidYArlTvlhwr3fEhYyM";
const param = "?api_key=" + key;

const ImageOfDay = React.createClass({
    getInitialState: function (){
        return {
            imgSrc: ""
        }
    },
    componentDidMount: function (){
        // sample of how GET request should look from NASA site
        // https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY
        axios.get('https://api.nasa.gov/planetary/apod' + param)
          .then(function(response){
                this.setState({
                    imgSrc: response.data.hdurl
                });
          }.bind(this));
    },
    render: function() {
        return (
            <div className="col-sm-12 text-center">
                <Header title="The Red Planet Rovers" btnOneTitle="Image of the Day" btnTwoTitle="Image Gallery"/>
                <h1>Image of the Day</h1>
                <img style={styles.image} src={this.state.imgSrc}/>
            </div>
        )
    }
});

module.exports = ImageOfDay;
