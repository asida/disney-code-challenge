const React = require('react');
const transparentBg = require('../styles').transparentBg;
const ReactRouter = require('react-router');
const Header = require('./Header');
const Link = ReactRouter.Link;


const Home = React.createClass({
    render: function(){
        return(
            <div className="app-header col-sm-12 text-center" style={transparentBg}>
                <Header title="The Red Planet Rovers" btnOneTitle="Image of the Day" btnTwoTitle="Image Gallery"/>
            </div>
        )
    }
});

module.exports = Home;
