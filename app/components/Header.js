const React = require('react');
const ReactRouter = require('react-router');
const styles = require('../styles');
const Link = ReactRouter.Link;


const Header = React.createClass({
    render: function(){
        return(
            <header className="col-sm-12 text-center" style={styles.appHeader}>
                <h1>{this.props.title}</h1>
                <div style={styles.buttonContainer}>
                    <Link to='/imageOfDay'>
                        <button type="button" className="btn btn-lg btn-success">{this.props.btnOneTitle}</button>
                    </Link>
                    <Link to='/imageGallery'>
                        <button type="button" className="btn btn-lg btn-success">{this.props.btnTwoTitle}</button>
                    </Link>
                </div>
            </header>
        )
    }
});

module.exports = Header;
