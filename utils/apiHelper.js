const axios = require('axios');

const key = "aZlmHCp3jD9sanwE8KvytidYArlTvlhwr3fEhYyM";
const param = "?api_key=" + key;

// sample of how GET request should look from NASA site
// https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY

const helpers = {
    getImageoftheDay: function () {
        axios.get('https://api.nasa.gov/planetary/apod' + param)
          .then(function(response){
            console.log('1',response.data);
            return response.data;
          });
    }
};

module.exports = helpers;
